# recalbox-themes

The recalbox emulationstation themes!

# License

The following themes are released under [Creative Commons Attribution-NonCommercial-NoDerivs 4.0 License CC BY-NC-ND](https://creativecommons.org/licenses/by-nc-nd/4.0/) ![](https://i.creativecommons.org/l/by-nc-nd/4.0/88x31.png):
* recalbox
* recalbox-next
* recalbox-multi
* DarKade

The following themes are on an unknown license or yet to be decided:
* Blisslight
* eudora
* eudora-bigshot
* eudora-concise
* pixel

# Systems list to support:

Find below the systems list to support to obtain a theme compatible with recalboxOS.

| Systems | Folder's Name | Recalbox's Version |
| :--------------: | :--------------: | :--------------: |
| Amstrad cpc | amstradcpc | 4.0.0 |
| Atari 2600 | atari2600 | 4.0.0 |
| Atari 7800 | atari7800 | 4.0.0 |
| Atari ST | atarist | 4.0.0 |
| CaveStory | cavestory | 4.0.0 |
| Favoris | favorites | 4.0.0 |
| FDS (Family Computer Disk) | fds | 4.0.0 |
| Final Burn Alpha | fba | 4.0.0 |
| Final Burn Alpha Libretro | fba_libretro | 4.0.0 |
| Game & Watch | gw | 4.0.0 |
| Game boy color | gbc | 4.0.0 |
| Game Gear | gamegear | 4.0.0 |
| Gameboy | gb | 4.0.0 |
| Gameboy  Advance | gba | 4.0.0 |
| Image Viewer | imageviewer | 4.0.0 |
| Lutro | lutro | 4.0.0 |
| Lynx | lynx | 4.0.0 |
| Mame | mame | 4.0.0 |
| Master System | mastersystem | 4.0.0 |
| Megadrive | megadrive | 4.0.0 |
| Moonlight | moonlight | 4.0.0 |
| MS X 1-2-2+ | msx | 4.0.0 |
| MSX1 | msx1 | 4.0.0 |
| MSX2+ | msx2 | 4.0.0 |
| Neo Geo | neogeo | 4.0.0 |
| Neo Geo Pocket B&W | ngp | 4.0.0 |
| Neo Geo Pocket Color | ngpc | 4.0.0 |
| Nintendo | nes | 4.0.0 |
| Nintendo 64 | n64 | 4.0.0 |
| Odyssey 2 | odyssey2 | 4.0.0 |
| PC Engine | pcengine | 4.0.0 |
| PC Engine CD | pcenginecd | 4.0.0 |
| Playstation | psx | 4.0.0 |
| PR Boom | prboom | 4.0.0 |
| Scumm VM | scummvm | 4.0.0 |
| Sega 32 X | sega32x | 4.0.0 |
| Sega CD | segacd | 4.0.0 |
| Sega SG 1000 | sg1000 | 4.0.0 |
| Super Nintendo | snes | 4.0.0 |
| Supergrafx | supergrafx | 4.0.0 |
| Vectrex | vectrex | 4.0.0 |
| Virtual Boy | virtualboy | 4.0.0 |
| Wonderswan B&W | wonderswan | 4.0.0 |
| Wonderswan Color | wonderswancolor | 4.0.0 |
| ZX Spectrum | zxspectrum | 4.0.0 |
| ZX81 | z81 | 4.0.0 |
|   |   |   |
| Apple II | apple2 | 4.1.0 |
| Colecovision | colecovision | 4.1.0 |
| Commodore 64 | c64 | 4.1.0 |
| DosBox | pc | 4.1.0 |
| Dreamcast | dreamcast | 4.1.0 |
| Gamecube | gc | 4.1.0 |
| Playstation portable | psp | 4.1.0 |
| Wii | wii | 4.1.0 |
|   |   |   |
| 3DO Interactive Multiplayer | 3do | 2018.02.09 |
| Amiga 600 | amiga1200 | 2018.02.09 |
| Amiga 1200 | amiga600 | 2018.02.09 |
| Nintendo DS | nds | 2018.02.09 |
| Sharp X68000 | x68000 | 2018.02.09 |
|   |   |   |
| Daphne | daphne | 2018.06.27 |
| Thomson TO8 | to8 | 2018.06.27 |
|   |   |   |
| Atari 800 | atari800 | 2019.0 |
| Atari 5200 | atari5200 | 2019.0 |
| Mattel Intellivision | intellivision | 2019.0 |
| Atari Jaguar | jaguar | 2019.0 |
| Satellaview | satellaview | 2019.0 |
| Neo Geo CD | neogeocd | 2019.0 |
| Bandai SuFami Turbo | sufami | 2019.0 |
| Pokémon Mini | pokemini | 2019.0 |
| Sega NAOMI | naomi | 2019.0 |
| Sammy Atomiswave | atomiswave | 2019.0 |
| Fairchild Channel F | channelf | 2019.0 |
| Tangerine Oric | oric | 2019.0 |
| MGT SAM Coupé | samcoupe | 2019.0 |
| NEC PC-FX | pcfx | 2019.0 |
| Nintendo 3DS | 3ds | 2019.0 |
| Amiga CD32 | amigacd32 | 2019.0 |
| NEC PC-98 | pc98 | 2019.0 |
|   |   |   |
| Sega Saturn | saturn | 2019.0 |
| Amiga CDTV | amigacdtv | 201X.0 |
| Ports | ports | 201X.0 |